package com.example.homeworkspringapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/Api")
public class HomeworkSpringApiApplication {
    ArrayList <Customer> customers = new ArrayList<> ();
    static int counter = 4;

    //ADD Customer to list Function
    public HomeworkSpringApiApplication() {
        customers.add(new Customer (1,"Vattana","M","PP"));
        customers.add(new Customer (2,"Namheng","M","SR"));
        customers.add(new Customer (3,"Thary","F","BTB"));
    }
    // URL Show ALL Customer List
    @GetMapping("/Customer/Show")
    public ArrayList<Customer> showCustomer(){
        return customers;
    }

    //Insert Customer Function
    @PostMapping("/Customer/Post")
    public Customer customer1(@RequestBody Customer customer){
        customer.setId(counter++);
        customers.add(customer);
        return customer;
    }
    //Read Customer by ID Function
    @GetMapping("/Customer/SearchbyID{id}")
    public ResponseEntity<Customer> customer (@PathVariable int id){
        for (Customer customer: customers
             ) {
            if(customer.getId()==id){
                 return new ResponseEntity<>(customer, HttpStatus.OK);
            };
        } return null;
    }

    //Read Customer by Name Function
    @GetMapping("/Customer/Name/Search")
    @ResponseBody
    public ResponseEntity<Customer> customer(@RequestParam String name) {
        for (Customer customer: customers
        ) {
            if(customer.getName().equalsIgnoreCase(name)){
                return new ResponseEntity<>(customer, HttpStatus.OK);
            };
        } return null;
    }

    // Update Customer by ID
    @PutMapping("/Customer/{id}")
     public ResponseEntity<?> upCustomerID (@PathVariable int id,@RequestBody Customer customerUpdate){
        for (Customer customer: customers
        ) {
            if(customer.getId () == id){
                customers.set(id-1,customerUpdate);
                return new ResponseEntity <>(customerUpdate, HttpStatus.OK);
            };
        }
        return null;

    }

    // Delete Customer by ID
    @DeleteMapping("/Customer/Delete/{id}")
    public ResponseEntity<Customer> delCustomerID(@PathVariable int id){
            Customer customer = customers.remove(id-1);
            if (customer == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    public static void main(String[] args) {
        SpringApplication.run (HomeworkSpringApiApplication.class, args);
    }

}
